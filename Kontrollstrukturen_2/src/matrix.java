import java.util.*;

public class matrix {
    public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int zahl = eingabe.nextInt();

		matrixGenerator(zahl);

		eingabe.close();
	}
	
	public static void matrixGenerator(int teilZahl) {
		
		int i = 0;
		while (i < 100) {
			
			int stelle1 = i / 10;
			int stelle2 = i % 10;
			
			
			if (i % teilZahl == 0) //prüft ob i durch die eingegebene Zahl teilbar ist
				System.out.printf("%4s", "*");
			
			else if ( stelle1 == teilZahl || stelle2 == teilZahl) // ziffer enthalten
				System.out.printf("%4s", "*");
			
			else if ( stelle1 + stelle2 == teilZahl) // quersumme
				System.out.printf("%4s", "*");
			
			else 
				System.out.printf("%4d", i);
			
			i++;
			
			if (i % 10 == 0 && i != 0) //neue zeile
				System.out.println();
	
		}
	}
}
