import java.util.*;

public class Taschenrechner {

    static Scanner eingabe = new Scanner(System.in);
    
    public static void main(String[] args) {
		
		double zahl1 = userDouble("Geben Sie die erste Zahl ein");
		double zahl2 = userDouble("Geben Sie die zweite Zahl ein");
		char operator = userChar("Geben Sie den Rechenoperator ein");
		System.out.println(rechner(zahl1, zahl2, operator));

		eingabe.close();
	}
	
	public static double userDouble (String eingabeAufforderung) {
		System.out.println(eingabeAufforderung);
		return eingabe.nextDouble();
	}
	
	public static char userChar (String eingabeAufforderung) {
		System.out.println(eingabeAufforderung);
		return eingabe.next().charAt(0);
	}
	
	public static double rechner(double zahl1, double zahl2, char rechenzeichen) {
		double ergebnis = 0;
		switch (rechenzeichen) {
		case '+' :
			ergebnis = zahl1 + zahl2;
			break;
		case '-' :
			ergebnis = zahl1 - zahl2;
			break;
		case '*' :
			ergebnis = zahl1 * zahl2;
			break;
		case '/' :
			ergebnis = zahl1 / zahl2;
			break;
		default:
			System.out.println("Kein gültiger Operator");
		}	
		return ergebnis;
	}
}
