public class Einmaleins {
    public static void main(String[] args) throws Exception {
        int vertical = 1;
        int m = 1;

        for (int i = 0; i < 10; i++){
            for (int k = 0; k < 10; k++){
                int num = vertical * m;
                System.out.print(num);
                if (num > 9 || vertical == 10){
                    System.out.print(" ");
                } else{
                    System.out.print("  ");
                }
                vertical++;
            }
            vertical = 1;
            System.out.print("\n");
            m++;
        }
    }
}
