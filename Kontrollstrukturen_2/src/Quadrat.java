import java.util.*;

public class Quadrat {
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.print("Größe: ");
		int dimention = sc.nextInt();
		int initialDimention = dimention;

		for (int i = 0; i < dimention; i++){
			if (initialDimention == dimention || initialDimention == 0){
				System.out.println("* ".repeat(dimention));
				initialDimention--;
			} else {
				System.out.println("* " + "  ".repeat(dimention-2) + "*");
			}
			initialDimention--;
		}
		sc.close();
	}
}
