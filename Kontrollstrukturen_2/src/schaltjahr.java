import java.util.*;

public class schaltjahr {

    static Scanner eingabe = new Scanner(System.in);
    
    public static void main(String[] args) {
		
		int jahreszahl = userInt("Geben Sie eine Jahreszahl ein");
		System.out.println(schaltJahrCheck(jahreszahl));
	}
	
	public static int userInt (String eingabeAufforderung) {
		System.out.println(eingabeAufforderung);
		return eingabe.nextInt();
	}
	
	public static String schaltJahrCheck(int jahreszahl) {
		
		String schaltjahrodernicht = "";
		
		if(jahreszahl % 4 == 0) {
			if(jahreszahl >= 1582) {
				if(jahreszahl % 100 == 0) {
					if(jahreszahl % 400 == 0) {
						schaltjahrodernicht = "Schaltjahr :)";
					} else {
						schaltjahrodernicht = "Kein Schaltjahr :(";
					}		
				} else {
					schaltjahrodernicht = "Schaltjahr :)";
				}
			} else {
				schaltjahrodernicht = "Kein Schaltjahr :(";
			}
		} else {
			schaltjahrodernicht = "Kein Schaltjahr :(";
		}
		return schaltjahrodernicht;
	}
}
