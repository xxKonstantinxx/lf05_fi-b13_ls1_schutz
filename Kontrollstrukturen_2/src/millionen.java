import java.util.*;

public class millionen {
    public static void main(String[] args) {
        int jahre = 0;
        float zinsSatz;
        float einzahlung;
        Scanner sc = new Scanner(System.in);
        boolean weiter = true;

        while (weiter){
            System.out.print("Einzahlung: ");
            einzahlung = sc.nextFloat();
            System.out.print("\nZinssatz: ");
            zinsSatz = sc.nextFloat();

            while (einzahlung < 1000000){
                einzahlung = einzahlung * ((zinsSatz / 100) +1);
                jahre++;
            }

            System.out.print("In ");
            System.out.print(jahre);
            System.out.println(" Jahren bist du millionär 🥳");

            System.out.println("weiter? (j/n)");
            String jo = sc.next();
            if (jo.equals("n")){
                break;
            }

        }

        sc.close();


    }
}
