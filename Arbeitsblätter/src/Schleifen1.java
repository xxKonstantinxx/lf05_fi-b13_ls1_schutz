import java.util.*;

public class Schleifen1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        for (int zähler = 1; zähler <= n; zähler++){
            System.out.println(zähler);
        }
        //rückwärts
        // for (int zähler = 1; n > zähler; n-- ){
        //     System.out.println(n);
        // }
        sc.close();
    }
}
