import java.util.*;

public class Auswahlstrukturen {
    
    public static void main(String[] args) {
        
        int kopfhörerLadung = 78;
        int handyLadung = 87;

        if (kopfhörerLadung == handyLadung){
            System.out.println("yaay");
        } else if (handyLadung > kopfhörerLadung){
            System.out.println("case 1");
        } else if (kopfhörerLadung > handyLadung){
            System.out.println("case 2");
        } else {
            System.out.println("error");
        };

        Scanner sc = new Scanner(System.in);

        System.out.println("zahl 1");
        int zahl1 = sc.nextInt();
        System.out.println("zahl 2");
        int zahl2 = sc.nextInt();
        System.out.println("zahl 3");
        int zahl3 = sc.nextInt();

        if (zahl1 > zahl2 && zahl1 > zahl3){
            System.out.println("zahl 1 ist größer als zahl 2 und 3");
        } else if (zahl3 > zahl2 || zahl3 > zahl1){
            System.out.println("zahl 3 ist größer als zahl 1 oder 2");
        } else {
            System.out.println("error");
        }

        if ( zahl1 > zahl2 && zahl1 > zahl3){
            System.out.println("zahl 1 ist am größten");
        } else if (zahl2 > zahl1 && zahl2 > zahl3){
            System.out.println("zahl 2 ist am größten");
        } else if (zahl3 > zahl1 && zahl3 > zahl2){
            System.out.println("zahl 3 ist am größten");
        } else {
            System.out.println("es gibt keine eindeutig größte zahl");
        }

        sc.close();
    };


};
