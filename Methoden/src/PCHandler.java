import java.util.*;

public class PCHandler {

	public static void main(String[] args) {
		// Scanner myScanner = new Scanner(System.in);

		// // Benutzereingaben lesen
		// System.out.println("was m�chten Sie bestellen?");
		// String artikel = myScanner.next();

		// System.out.println("Geben Sie die Anzahl ein:");
		// int anzahl = myScanner.nextInt();

		// System.out.println("Geben Sie den Nettopreis ein:");
		// double preis = myScanner.nextDouble();

		// System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		// double mwst = myScanner.nextDouble();

        String wtf = liesString("Was möchten sie bestellen?");
        System.out.println(wtf);

		// // Verarbeiten
		// double nettogesamtpreis = anzahl * preis;
		// double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// // Ausgeben

		// System.out.println("\tRechnung");
		// System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		// System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

    public static String liesString(String text){
        Scanner sc = new Scanner(System.in).useDelimiter("\n");
        System.out.print(text);
        String scan = sc.next();
        sc.close();
        return scan;
    }

    public static int liesInt(String text){
        Scanner sc = new Scanner(System.in);
        System.out.print(text);
        int scan = sc.nextInt();
        sc.close();
        return scan;
    }

    public static double liesDouble(String text){
        Scanner sc = new Scanner(System.in);
        System.out.print(text);
        double scan = sc.nextDouble();
        sc.close();
        return scan;
    }

    public static double berechneGesamtnettopreis(int anzahl, double
    nettopreis){
        return(nettopreis * anzahl);
    }

    public static double berechneGesamtbruttopreis(double nettogesamtpreis,
    double mwst){
        return (nettogesamtpreis * (1 + mwst / 100));
    }

    public static void rechungausgeben(String artikel, int anzahl, double
    nettogesamtpreis, double bruttogesamtpreis, double mwst){
        System.out.println("Artikel: " + artikel);
        System.out.println("Anzahl: " + anzahl);
        System.out.println("Netto: " + nettogesamtpreis);
        System.out.println("Brutto: " + bruttogesamtpreis);
        System.out.println("mwst: " + mwst);
    }

    public static double reihenschaltung(double r1, double r2){
        return (r1+r2);
    }

    public static double parallelschaltung(double r1, double r2){
        return ((r1*r2)/(r1+r2));
    }
    
}