package Arbeitsblatt2;
public class Aufgabe_3 {

    public static void main(String[] args) {
        

        String[] celsius = { "-20", "-10", "+0", "+20", "+30"};

        double[] fahrenheit = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
        
        for (int i=0;i<fahrenheit.length;i++) {
            fahrenheit[i] = Math.round(fahrenheit[i] * 100.0) / 100.0;
       }


        final Object[][] tabelle = new String[7][];
        tabelle[0] = new String[] { "Fahrenheit", "|", "Celsius" };
        tabelle[1] = new String[] { "-----------------------", "", ""};
        tabelle[2] = new String[] { String.valueOf(celsius[0]), "|", String.valueOf(fahrenheit[0]) };
        tabelle[3] = new String[] { String.valueOf(celsius[1]), "|", String.valueOf(fahrenheit[1]) };
        tabelle[4] = new String[] { String.valueOf(celsius[2]), "|", String.valueOf(fahrenheit[2]) };
        tabelle[5] = new String[] { String.valueOf(celsius[3]), "|", String.valueOf(fahrenheit[3]) };
        tabelle[6] = new String[] { String.valueOf(celsius[4]), "|", String.valueOf(fahrenheit[4]) };

        for (final Object[] row : tabelle) {

        System.out.format("%-12s%s%10s%n", row);
}
    }
}
