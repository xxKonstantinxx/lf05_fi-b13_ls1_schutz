public class App {
    public static void main(String[] args) throws Exception {

        Raumschiff[] raumschiffe = raumschiffInitialisierung();
        Raumschiff enterprise = raumschiffe[0];
        Raumschiff borgCubus = raumschiffe[1];
        getRaumschiffInfo(enterprise);
        getVitals(enterprise);
        System.out.println("\n\n");
        getRaumschiffInfo(borgCubus);
        getVitals(borgCubus);
    }

    public static class Raumschiff {
        public Captain captain;
        public int energie;
        public int lebenserhaltung;
        public int schilde;
        public int hülle;
        public int reperaturAndroiden;
        public String bezeichnung;
        public Ladung[] ladung;
        public Waffe[] waffen;

        public Raumschiff(String bezeichnung, int energie, int lebenserhaltung, int schilde, int hülle,
                int reperaturAndroiden, Captain captain, Ladung[] ladung, Waffe[] waffe) {
            this.energie = energie;
            this.lebenserhaltung = lebenserhaltung;
            this.schilde = schilde;
            this.hülle = hülle;
            this.reperaturAndroiden = reperaturAndroiden;
            this.bezeichnung = bezeichnung;
            this.captain = captain;
            this.ladung = ladung;
        }
    }

    public static class Captain {
        public String name;
        public int captainSeit;

        public Captain(String name, int captainSeit) {
            this.name = name;
            this.captainSeit = captainSeit;
        }
    }

    public static class Ladung {
        String name;
        int anzahl;
        String verwendungsZweck;

        public Ladung(String name, int anzahl, String verwendungszweck) {
            this.name = name;
            this.anzahl = anzahl;
            this.verwendungsZweck = verwendungszweck;
        }
    }

    public static class Waffe {
        String name;
        int anzahl;

        public Waffe(String name, int anzahl) {
            this.name = name;
            this.anzahl = anzahl;
        }
    }

    public static Raumschiff[] raumschiffInitialisierung() {
        Captain picard = new Captain("Jean-Luc picard", 2364);
        Ladung[] enterpriseLadung = { new Ladung("Tricorder", 120, "Forschung"),
                new Ladung("Sonden", 200, "Forschung") };
        Waffe[] enterpriseWaffen = { new Waffe("Photonentorpedos", 1) };
        Captain borgKoenigin = new Captain("Borg Königin", 1778);
        Ladung[] borgLadung = { new Ladung("Klingonen", 200, "Assimilierung"),
                new Ladung("Romulaner", 300, "Assimilierung"), new Ladung("Vulkanier", 50, "Assimilierung") };
        Waffe[] borgWaffen = {};
        Raumschiff[] raumschiffe = {
                new Raumschiff("Enterprise", 100, 100, 100, 3, 0, picard, enterpriseLadung, enterpriseWaffen),
                new Raumschiff("Borg Cubus", 120, 100, 100, 100, 3, borgKoenigin, borgLadung, borgWaffen) };
        return raumschiffe;
    }

    public static void getRaumschiffInfo(Raumschiff raumschiff) {
        System.out.println(raumschiff.bezeichnung);
        System.out.print("Captain: ");
        System.out.println(raumschiff.captain.name + "\nIm amt seit " + raumschiff.captain.captainSeit + "\n");
        System.out.println("Ladung:");
        for (int i = 0; i < raumschiff.ladung.length; i++) {
            System.out.println(raumschiff.ladung[i].name + ", anzahl: " + raumschiff.ladung[i].anzahl
                    + ", verwendungszweck: " + raumschiff.ladung[i].verwendungsZweck);
        }
    }

    public static void getVitals(Raumschiff raumschiff) {
        System.out.println("Energie: " + raumschiff.energie);
        System.out.println("Schilde: " + raumschiff.schilde);
        System.out.println("Hülle: " + raumschiff.hülle);
        System.out.println("Waffen:");
        if (raumschiff.waffen != null) {
            for (int i = 0; i < raumschiff.waffen.length; i++) {
                System.out.println(raumschiff.waffen[i].name + ", " + raumschiff.waffen[i].anzahl);
            }
        } else {
            System.out.print("keine");
        }

    }

}