﻿import java.util.*;

class Fahrkartenautomat {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int ticketCount = 500;
        int zweiEuro = 100;
        int einEuro = 100;
        int fuenfzigCent = 100;
        int zwanzigCent = 100;
        int zehnCent = 100;
        int fuenfZent = 100;

        while (true) {
            double[] bestellung = fahrkartenbestellungErfassen();
            if (bestellung[0] == 0 && bestellung[1] == 0) {
                int[] bestand = adminMenue(ticketCount, zweiEuro, einEuro, fuenfzigCent, zwanzigCent, zehnCent,
                        fuenfZent);
            }
            double betrag = bestellung[0];
            int anzahl = (int) bestellung[1];
            double eingezahlterGesamtbetrag = fahrkartenBezahlen(betrag);
            fahrkartenAusgeben(anzahl);
            double rückgabebetrag = Math.round((eingezahlterGesamtbetrag - betrag) * 100.0) / 100.0;
            rueckgeldAusgeben(rückgabebetrag);
        }
    }

    public static double[] fahrkartenbestellungErfassen() {
        // Da ich in meiner letzten version des Fahrkartenautomates schon arrays
        // benutzt habe, habe ich in dieser version einen eiges Fahrkarten Objekt
        // implementiert und daraus ein Array gemacht.
        // Die vorteile sind das Schnellere einfügen von Fahrkarten und strukturierterer
        // code.
        // Außerdem ist diese variante weniger Leistungsintensiv.

        double[] arr = new double[2];
        Fahrkarte[] fahrkarten = getFahrkarten();
        double kartenAnzahl = 0;
        double zuZahlenderBetrag = 0;
        boolean isValid = false;
        boolean isAdmin = false;
        int karte;

        do {
            System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
            for (int i = 0; i < fahrkarten.length; i++) {
                System.out.println(fahrkarten[i].auswahlNummer + " " + fahrkarten[i].bezeichnung + ", "
                        + fahrkarten[i].preis + "0€");
            }
            System.out.println("11 exit");
            System.out.print("> ");
            karte = sc.nextInt();
            if (karte >= 1 && karte <= fahrkarten.length) {
                zuZahlenderBetrag = fahrkarten[karte - 1].preis;
                isValid = true;
            } else if (karte == 11) {
                isValid = true;
                isAdmin = true;
            } else {
                System.out.println(">>falsche Eingabe<<");
            }
        } while (isValid != true);
        if (!isAdmin) {
            System.out.print("Wie viele Fahrkarten sind Gewünscht? ");
            kartenAnzahl = sc.nextDouble();
        }
        arr[0] = zuZahlenderBetrag * kartenAnzahl;
        arr[1] = kartenAnzahl;
        return (arr);
    }

    public static double fahrkartenBezahlen(double betrag) {
        double eingezahlterGesamtbetrag = 0.00;
        while (eingezahlterGesamtbetrag < betrag) {
            System.out.print("Noch zu zahlen: ");
            System.out.printf("%.2f", (betrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = sc.nextDouble();
            if (eingeworfeneMünze <= 2.0 && eingeworfeneMünze >= 0.05) {
                eingezahlterGesamtbetrag += eingeworfeneMünze;
            } else {
                System.out.println("mind. 5Ct, höchstens 2 Euro!");
            }
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben(int anzahl) {
        for (int i = 0; i < anzahl; ++i) {
            System.out.println("\nFahrschein wird ausgegeben");
            for (int n = 0; n < 8; n++) {
                System.out.print("=");
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("\n\n");
        }
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {
        if (rückgabebetrag > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("    * * *\n  *       *\n *    2    *\n *   Euro  *\n  *       *\n    * * *");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("    * * *\n  *       *\n *    1    *\n *   Euro  *\n  *       *\n    * * *");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("    * * *\n  *       *\n *    50   *\n *   Cent  *\n  *       *\n    * * *");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("    * * *\n  *       *\n *    20   *\n *   Cent  *\n  *       *\n    * * *");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("    * * *\n  *       *\n *    10   *\n *   Cent  *\n  *       *\n    * * *");
                rückgabebetrag -= 0.1;
            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("    * * *\n  *       *\n *    5    *\n *   Cent  *\n  *       *\n    * * *");
                rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.\n");
    }

    public static class Fahrkarte {
        public int auswahlNummer;
        public double preis;
        public String bezeichnung;

        public Fahrkarte(int auswahlNummer, double preis, String bezeichnung) {
            this.auswahlNummer = auswahlNummer;
            this.preis = preis;
            this.bezeichnung = bezeichnung;
        }
    }

    public static Fahrkarte[] getFahrkarten() {
        Fahrkarte[] fahrkarten = new Fahrkarte[10];
        fahrkarten[0] = new Fahrkarte(1, 2.9, "Einzelfahrschein Berlin AB");
        fahrkarten[1] = new Fahrkarte(2, 3.3, "Einzelfahrschein BC");
        fahrkarten[2] = new Fahrkarte(3, 3.6, "Einzelfahrschein ABC");
        fahrkarten[3] = new Fahrkarte(4, 1.9, "Kurzstrecke");
        fahrkarten[4] = new Fahrkarte(5, 8.6, "Tageskarte Berlin AB");
        fahrkarten[5] = new Fahrkarte(6, 9.0, "Tageskarte Berlin BC");
        fahrkarten[6] = new Fahrkarte(7, 9.6, "Tageskarte Berlin ABC");
        fahrkarten[7] = new Fahrkarte(8, 23.5, "Kleingruppen-Tageskarte Berlin AB");
        fahrkarten[8] = new Fahrkarte(9, 24.3, "Kleingruppen-Tageskarte Berlin BC");
        fahrkarten[9] = new Fahrkarte(10, 24.9, "Kleingruppen-Tageskarte Berlin ABC");

        return fahrkarten;
    }

    // das soll das administrations menü werden, leider have ich dafür keine zeit
    // mehr
    // daher dient es erstmal als exit
    public static int[] adminMenue(int ticketCount, int zweiEuro, int einEuro, int fuenfzigCent, int zwanzigCent,
            int zehnCent, int fuenfZent) {

        int[] rere = new int[5];
        System.exit(0);
        return rere;
    }
}